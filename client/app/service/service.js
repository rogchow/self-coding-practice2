(function(){
    angular
        .module("Testing")
        .service("Testservice", Testservice);


    Testservice.$inject = ["$http"];

    function Testservice($http){
        var service = this;
        service.testInsert = testInsert;

        function testInsert(testInput1){
            return $http({
                method: 'POST',
                url: "/test/test",
                data: {input: testInput1}
            })
        }
    };
})()