(function () {
    angular
        .module("Testing")
        .config(testroutingConfig);
    testroutingConfig.$inject = ["$stateProvider","$urlRouterProvider"];

    function testroutingConfig($stateProvider,$urlRouterProvider){
        $stateProvider
            .state('A',{
                url : '/testcase1',
                templateUrl: './app/Testcase1/testcase1.html',
                controller : 'testCtrl1',
                controllerAs : 'Ctrl'
            })
            .state('B', {
                url: '/testcase2',
                templateUrl: './app/Testcase2/testcase2.html',
                controller : 'testCtrl2',
                controllerAs : 'Ctrl'
            })

    $urlRouterProvider.otherwise("/A");
}

})()