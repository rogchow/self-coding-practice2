(function(){
    angular
        .module("Testing")
        .controller("testCtrl1", testCtrl1);

 testCtrl1.$inject = [ '$window', '$state', '$filter','Testservice'];

    function testCtrl1($window, $state, $filter, Testservice){
        var vm = this;

        vm.testInput1 = {
            name: "",
            age: ""
        };


        vm.testButton1 = testButton1;

        function testButton1(){
            console.log("testButton working....!");
            Testservice.testInsert(vm.testInput1)
            .then (function (result){
                    console.log()
                    $window.location.assign('/app/Testcase1/thanks.html');
            }).catch(function(err){
                    console.log("error " + JSON.stringify(err));
                    //vm.status.message = err.data.name;
                    //vm.status.code = err.data.parent.errno;
            })
            
        };
}

})();
