var express = require("express");
var bodyParser = require("body-parser");
var path = require("path");


const NODE_PORT = process.env.NODE_PORT || 3000;
const CLIENT_FOLDER = path.join(__dirname, "../client");
//const IMG_FOLDER = path.join(__dirname, "../images");
//const MSG_FOLDER = path.join(__dirname, "../client/assets/messages");
const MYSQL_USERNAME = "root";
const MYSQL_PASSWORD = "MyNewPass"; 

const API_FRIENDS_ENDPOINT = "/api/friends";
const API_GROUPS_ENDPOINT = "/api/groups";

var app = express();

app.use(express.static(CLIENT_FOLDER));
//app.use("/uploads", express.static(IMG_FOLDER));
//app.use(bodyParser.json());
//app.use(bodyParser.urlencoded({extended: false}));

// Routes
//require("./route")(app);


app.post("/test/test", function (req, res) {
    console.log("executed up to here");
    console.log("Received User Object",req.body);
    res.status(200).json(req.body.testInput1);
});

app.listen(NODE_PORT, function() {
    console.log("App started at port: %s", NODE_PORT);
});